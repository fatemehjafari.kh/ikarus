FROM ubuntu:20.04

RUN apt update -y
RUN apt -y install unzip curl

RUN dpkg --add-architecture i386
RUN apt-get install --no-install-recommends --assume-yes wine

WORKDIR /home/unscanned-files
COPY 1.txt .

RUN rm -rf /var/lib/apt/lists/*

RUN curl http://updates.ikarus.at/cgi-bin/t3download.pl/ikarust3scan_w64.exe -o ikarust3scan_w64.exe 
RUN curl http://updates.ikarus.at/cgi-bin/t3download.pl/t3sigs.vdb -o t3sigs.vdb 

RUN unzip ikarust3scan_w64.exe

